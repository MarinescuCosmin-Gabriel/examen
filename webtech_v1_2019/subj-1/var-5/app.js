function textProcessor(input, tokens) {
    function isString(obj) {
        return (Object.prototype.toString.call(obj) === '[object String]');
    }

    if (isString(input) && input.length < 6) {
        throw 'Input should have at least 6 characters'

    } else
        if (!(isString(input))) {
            throw 'Input should be a string'
        } else {
            if (Array.isArray(tokens)) {
                console.log(input);
                for (let i = 0; i < tokens.length; i++) {
                    if (!(typeof tokens[i].tokenName === 'string') && !(typeof tokens[i].tokenValue === 'string')) {
                        throw 'Invalid array format'
                    } else {
                        if (!(input.includes("${" + "name" + "}")) && !(input.includes("${" + "surname" + "}"))) {
                            console.log(input)
                            return input
                        }
                        else {
                            if (input.includes("${" + "name" + "}")) {
                                input = input.replace("${" + "name" + "}", tokens[i].tokenValue)


                            }
                            if (input.includes("${" + "surname" + "}")) {
                                if (tokens[i].tokenName === "surname") {
                                    input = input.replace("${" + "surname" + "}", tokens[i].tokenValue)
                                }
                            }

                            console.log(input);
                            
                        }

                    }
                    
                }
                return input;

            }
        }
}

const app = {
    textProcessor: textProcessor
};

module.exports = app;