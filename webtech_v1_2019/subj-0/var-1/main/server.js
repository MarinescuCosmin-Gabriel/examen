const express = require("express")

const app = express()
app.use('/', express.static('public'))
var cors = require('cors')
app.use(cors());
app.listen(8080);

module.exports = app;